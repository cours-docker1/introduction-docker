# Introduction Docker

Dans ce TP nous allons voir l'installation de DOcker et les premiers exemples d'utilisation de votre nouvel environnement.

## Les bases

### Vérifier l'état Docker

les commandes de base pour connaitre l'état de Docker sont : 

```bash
docker info #affiche plein d'information sur l'engine avec lequel vous êtes en contact
docker ps # affiche les conteneurs en train de tourner
docker ps -a # afficher également les conteneurs arrêtés
```

## Créer et lancer container

Un conteneur est une instance en cours de fonctionnement ("vivante") d'une image.

```bash
docker run [-d] [-p port_host:port_conteneur] [-v dossier_host:dossier_conteneur] <image> <commande>
```

- L'ordre des arguments est important !
- Un nom est automatiquement généré pour le conteneur à moins de fixer le nom avec `--name`
- On peut facilement lancer autant d'instances que nécessaire tant qu'il n'y a pas de collision de `nom` ou de `port`

## Options docker run

- les options facultatives indiqués sont très couramment utilisées.

   - `-d` permet de lancer le conteneur en mode daemon ou détaché afin de libérer le terminal.
   - `-p` permet de mapper un port réseau entre l'intérieur et l'extérieur du conteneur, typiquement lorsqu'on veut accéder à l'application depuis l'hôte.
   - `-v` permet de monter un volume partagé entre l'hôte et le conteneur
   - `--rm` permet de supprimer le conteneur dès qu'il s'arrête
   - `-it` permet de lancer une commande en mode interactif (bash par exemple)

## Commandes Docker

 - Le démarrage d'un conteneur est lié à une commande.
 - Si le conteneur n'a pas de commande, il s'arrête dès qu'il a fini de démarrer.

```bash
 docker run debian # s'arrête tout de suite
```

 - Pour utiliser une commande on peut simplement l'ajouter à la fin de la commande run

```bash
 docker run -d debian bash -c "echo 'attendre 10s' && sleep 30" # s'arrête après 10 secondes
```

## Stopper et redémarrer un conteneur

 `docker run` créé un nouveau conteneur à chaque fois

 ```bash
 docker stop <nom_ou_id_conteneur> # ne détruit pas le conteneur
 docker start <nom_ou_id_conteneur> # le conteneur à déjà été créé
 ```
## Introspection de conteneur

 - La commande `docker exec` permet d'exécuter une commande à l'intérieur du conteneur s'il est lancé. Une utilisation typique est d'introspecter un conteneur en lançant `bash` ou `sh`

 ```bash
 docker exec -it -<nom_ou_id_conteneur> /bin/bash
 ```
# Installation de Docker sous Windows

Pour installer Docker rien de plus simple, il suffit d'utilisation l'installeur officiel en le téléchargeant.

[Windows](https://www.docker.com/products/docker-desktop/)

# Lancer Docker

Lancer Docker en tant qu'administrateur.

# Premier test

Maintenant qu'il est installé sur votre poste, rien de plus simple. Dans une console, entrez la commande suivante :

```bash
docker version
```

Vous devriez avoir ce genre de résultat:

```bash
Client: Docker Engine - Community
 Version:           26.1.3
 API version:       1.45
 Go version:        go1.21.10
 Git commit:        b72abbb
 Built:             Thu May 16 08:33:29 2024
 OS/Arch:           linux/amd64
 Context:           default

Server: Docker Engine - Community
 Engine:
  Version:          26.1.3
  API version:      1.45 (minimum version 1.24)
  Go version:       go1.21.10
  Git commit:       8e96db1
  Built:            Thu May 16 08:33:29 2024
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.6.32
  GitCommit:        8b3b7ca2e5ce38e8f31a34f35b2b68ceb8470d89
 runc:
  Version:          1.1.12
  GitCommit:        v1.1.12-0-g51d5e94
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
  ```

  Il est important de prendre l'habitude de bien lire ce que la console nous indique après avoir passé les commandes.


Avec l'aide du support, et de `--help` :

   - Lancez simplement un conteneur Debian, Que se passe t-il ?

   - Lancez un conteneur Debian en mode détaché avec la commande echo "Debian container"

   - Lancez `docker logs` avec l'id du container précédent ou son nom. vous devriez voir le résultat de la commande `echo` précédente.

   - Affichez la liste des conteneurs en cours d'execution.

   - Affichez la liste des conteneurs en cours d'execution et arrêtés.

   - Lancez un conteneur debian en mode détaché avec la commande sleep 3600

   - Réaffichez la liste des conteneurs qui tournent.

   - Tentez de stopper le conteneur.

On peut désigner un conteneur soit par le nom qu’on lui a donné, soit par le nom généré automatiquement, soit par son empreinte (toutes ces informations sont indiquées dans un `docker ps` ou `docker ps -a`).

   - Redémarrez le conteneur que vous venez de stopper.

   - Trouvez comment vous débarassez d'un conteneur récalcitrant (si nécessaire, relancez un conteneur avec la commande sleep 3600 en mode détaché) 

   - Tentez de lancer deux conteneurs avec le nom `debian_container`

   - Créez un conteneur avec le nom `debian 2`

   - Lancez un container debian en mode interactif (options -i -t ou -it) avec la commande /bin/bash et le nom `debian_interactif`

   - Explorer l'intérieur du conteneur: c'est un OS Linux Debian normal.

   